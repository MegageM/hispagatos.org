---
title: "Arch"
date: 2019-02-06T00:11:11+01:00
draft: true
---
# HOW TO - INSTALAR ARCH GNU/Linux
Manual para instalar Arch GNU/Linux.

## PRE-INSTALACIÓN
- LAYOUT DEL TECLADO

Por defecto viene la US. Los layouts disponibles vienen dentro del directorio /usr/share/kbd/keymaps/···/···/···.map.gz.
Para modificar el layout del teclado usaremos [loadkeys(1)][1]. Por ejemplo, poner el layout español de Dvorak:

```
	# Dentro de i386/dvorak/
	loadkeys dvorak-es.map.gz 
```

Las distintas fonts se encuentran en /usr/share/kbd/consolefonts. Se modifica con el comando [setfont(8)][2].
En este caso vamos a poner [lat9w-16][3]

```
	setfont lat9w-16.psfu.gz
```

[1]: https://jlk.fjfi.cvut.cz/arch/manpages/man/loadkeys.1
[2]: https://jlk.fjfi.cvut.cz/arch/manpages/man/setfont.8
[3]: https://alexandre.deverteuil.net/files/consolefonts/README.lat9

- COMPROBACIÓN DE ACCESO A INTERNET

Comprobar la conexión a internet con [ping][1]

```
	ping hispagatos.org
```

[1]: https://jlk.fjfi.cvut.cz/arch/manpages/man/ping.8

- CONFIGURAR EL RELOJ DEL SISTEMA

Fijar la hora del sistema de acuedo a tu zona horaria usando [timedatectl(1)][1]

```
	# Buscar tu Región/Ciudad más cercana
	timedatectl list-timezones
	# Fijar la hora
	timedatectl set-timezone Región/Ciudad
	# Asegurarse de que el reloj del sistema es preciso
	timedatectl set-ntp true 
	# Ver el status del servicio
	timedatectl status
```

[1]: https://jlk.fjfi.cvut.cz/arch/manpages/man/timedatectl.1

- PARTICIONES DEL DISCO

Usando [cfdisk][1]. Este es un ejemplo particiones hechas bajo MRB:

```
	# Tipo "dos"
	cfdisk /dev/sda

	/dev/sda1 * 	-> /boot 	100 MiB Primaria
	/dev/sda2 	-> /		 20 GiB	Primaria
	/dev/sda3 	-> 		  ~ GiB	Extendida
	   /dev/sda5 	-> [SWAP] 	  2 GiB
	   /dev/sda6 	-> /home 	  ~ GiB 
```

El símbolo __*__ indica **Booteable**. El caracter __~__ lo uso para decir "todo el espacio disponible".
Ahora se formatean esas particiones

```
	# boot, raiz, home
	mkfs.ext2 /dev/sda1
	mkfs.ext4 /dev/sda2
	mkfs.ext4 /dev/sda6
	# SWAP
	mkswap /dev/sda5
	swapon /dev/sda5
```

Y las montamos

```
	# raiz
	mount /dev/sda2 /mnt
	# boot (MRB)
	mkdir -pv /mnt/boot
	mount /dev/sda1 /mnt/boot
	# home
	mkdir -pv /mnt/home
	mount /dev/sda6 /mnt/home
```

[1]: https://jlk.fjfi.cvut.cz/arch/manpages/man/cfdisk.8

## INSTALACIÓN
- MIRROR LIST 

Para que la descarga de los paquetes básicos sea más corta, podemos hacer que los paquetes se descargen de los servidores más (geográficamente) cercanos a nosotros.
Esto se hará dentro de /etc/pacman.d/mirrorlist, cuanto más arriba, mayor prioridad

![mirrorlist](https://fotos.hispagatos.org/_data/i/upload/2019/02/14/20190214195232-23f30fed-me.png)

En mi caso tengo situado a Spain el primero y después otros tres países cercanos

- INSTALACIÓN DE PAQUETES BÁSICOS

Instalamos los paquetes bases en /mnt

```
	pacstrap /mnt base
```

Esto no incluye todas las herramientas que hay en la instalación live.

## CONFIGURACIÓN DEL SISTEMA
- FSTAB

Generaremos el fichero [fstab][1]

```
	genfstab -U /mnt >> /mnt/etc/fstab
```

[1]: https://wiki.archlinux.org/index.php/Fstab

- CHROOT

Cambiamos a [arch-chroot][1] dentro de /mnt

```
	arch-chroot /mnt
```

[1]: https://wiki.archlinux.org/index.php/Chroot

- TIME ZONE

Fijamos el time zone con este comando sustitiyendo *Región/Ciudad* como antes

```
	ln -sf /usr/share/zoneinfo/Región/City /etc/localtime
```

Usando [hwclock][1] generamos /etc/adjtime

```
	hwclock --systohc
```

[1]: https://jlk.fjfi.cvut.cz/arch/manpages/man/hwclock.8

- LOCALIZACIÓN

Es importante que carácteres como '€' puedan ser mostrados por pantalla ya que librerías como **glibc** los usan.
Para elegir el que más convenga, descomentamos la línea en /etc/locale.gen. Por ejemplo, vamos a usar el siguiente

```
	en_US.UTF-8 UTF-8
```

Y aplicamos los cambios

```
	locale-gen
```

En /etc/locale.conf podemos configurar varias variables como el idioma del sistema

```
	LANG=en_US.UTF-8
```

A su vez, en /etc/vconsole.conf se define el layout del teclado

```
	KEYMAP=dvorak-es
```

- CONEXIÓN A INTERNET

Creamos el fichero /etc/hostname el cual albergará el nombre del equipo.
Dentro de /etc/hosts escribimos esto

```
	127.0.0.1	localhost
	::1		localhost
	127.0.1.1	nombre_máquina.localdomain nombre_máquina
```

- ROOT PASWORD

Poner una contraseña al **root**

```
	passwd
```

- BOOT LOADER

Para poder arrancar el sistema, necesitaremos un Boot Loader. El que vamos a usar es GRUB

```
	pacman -S grub
	# Lo instalamos
	grub-install --target=i386-pc /dev/sda
```

Se genera el fichero de arranque del GRUB **grub.cfg** dentro de /boot/grub

```
	grub-mkconfig -o /boot/grub/grub.cfg
```

- REBOOT

Salimos de arch-chroot con **exit**. Opcionalmente podemos desmontar las particiones antes del reinicio con

```
	umount -R /mnt 
```

A la hora de reiniciar es importante remover el USB de donde se haya booteado el **archiso**.

Si todo está correcto, podrá loggearse como root y empezar a usar este sistema Arch.

![arch-logo](https://fotos.hispagatos.org/_data/i/upload/2019/02/14/20190214195231-67791f3f-me.png)

# BONUS
Cuando se inicie sesión no habrá internet. Primero, revise que **enp0sX** tiene

```
	ip address show
```

Con [dhcpcd][1] y sabiendo su **enp0sX** nos conectamos a internet

```
	dhcpcd enp0sX
```

[1]: https://wiki.archlinux.org/index.php/Dhcpcd

# CONTINUA APRENDIENDO
![Wiki de Arch GNU/Linux](https://wiki.archlinux.org/) 
