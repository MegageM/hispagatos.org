#example commands to create new posts test draft and build
#
hugo new post/new-post-name.md
hugo server --buildDrafts -t after-dark
hugo undraft content/post/ctf_linuxfestnorthwest.md

# CHECK if the .md file removed the DRAF=True to DRAF=False
# if not, then edit and remove it and git add
hugo -t after-dark

git add content/post/name_blog_post.md
git commit "I added new blogpost about..."
git push
