---
categories: tor
comments: true
date: 2014-07-07T00:00:00Z
title: Hispagatos Sponsor a new TOR bridge
url: /2014/07/07/hispagatos-sponsor-a-new-tor-bridge/
---

<p>We decided after our IRC meeting to donate some server space and participage in the <a href="https://www.eff.org/torchallenge/">TOR challange</a>, create a new TOR bridge.</p>
<p>You can find our server listed at: <a href="https://globe.torproject.org/#/bridge/A230E36B6A14A253A222C9668A8FA1E180F88350">Globe Hispagatos.org</a></p>
