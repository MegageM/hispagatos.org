+++
description = "35c3-Chaos-Communication-Congress"
draft = false
toc = false
categories = ["technology"]
tags = ["tech", "hacking", "hackers"]
title= "35c5_ChaosCommunication_Congress Spanish"
date= 2019-02-05T21:45:41-03:00
images = [
  "https://source.unsplash.com/category/technology/1600x900"
] # overrides the site-wide open graph image
+++
---
<center>![35c3 logo](https://fotos.hispagatos.org/upload/2019/02/04/20190204185306-380c65da.jpg "35c3 Logo")</center>

-----
Reporte de Hispagatos en 35c3!
===========================


En hispagatos teníamos previsto asistir un año más la que consideramos la mejor y más real 
conferencia de hackers en el planeta por muchos años consecutivos. Con sólo [HOPE](https://hope.net/) en NY por detrás.


Nos organizamos con muchos otros grupos afines bajo un nombre común llamado[1KOMONA](https://en.wikipedia.org/wiki/Kommune_1) en honor al edificio y  periódico del ala izquierda bajo el cual se formó el[CCC Chaos Computer Club](https://www.ccc.de/en/).

```
El CCC se fundó en Berlín el 12 de septiembre de 1981 en una mesa que anteriormente había pertenecido al Kommune 1 en las salas del periódico Die Tageszeitung de Wau Holland y otros, en previsión al prominente rol que la tecnología de la información desempeñaría en la forma en que la gente vive y se comunica.
```


se está convirtiendo en un colectivo internacional de grupos afines involucrados con las ideas centrales de 
cambiar el mundo con tecnología, hacking, justicia social, música, danza y arte. 
Puede visitar nuestro

[Komona](https://www.komona.org/) se está convirtiendo en un colectivo internacional de grupos afines involucrados con las ideas centrales de 
cambiar el mundo con tecnología, hacking, justicia social, música, danza y arte.  
Puedes visitar nuestro [main site](https://www.komona.org/)

![Komona Picture](https://www.komona.org/AA_sticker_episode1_ohnetypo_downscale_h1200.png)


Estos son algunos de los colectivos que participaron.

```
!decentral
F.U.C.K.
Freimeuter
Hispagatos - International Anarchist Hackers
Infoladen
Minibar
NoLog.cz IT kolektiv
Organ tempel
RCC Office
Reclaim Club Culture
Spieleberatung
```


La [Asamblea de Komona en la sede del CCC](https://events.ccc.de/congress/2018/wiki/index.php/Assembly:Komona) donde se pueden ver nuestros colectivos de activistas, luchadores por la libertad, 
anarquistas y hackers anarquistas, que estuvieron colaborando en el evento, informando y trabajando duro, con 
asambleas diarias llamadas : PL(A)NUM:



![Planum](https://fotos.hispagatos.org/_data/i/upload/2019/02/01/20190201023645-a13544c0-me.jpg)

![Anarchist hacker graffity](https://fotos.hispagatos.org/_data/i/upload/2019/02/01/20190201024122-fd30d31f-me.jpg)

![Komona Banner](https://fotos.hispagatos.org/_data/i/upload/2019/02/01/20190201023414-b8ab5ffb-me.jpg)



Puesto oficial de nuestra Asamblea:

https://events.ccc.de/congress/2018/wiki/index.php/Assembly:Komona


-----
Nuestro sitio del álbum de fotos
====================
[Puedes encontrar más fotos aquí](https://fotos.hispagatos.org/index.php?/category/25), ten en cuenta que la mayoría de las fotos se hacen muy tarde, antes y después del evento para respetar la privacidad de nuestros compañeros, pero leyendo las pancartas y los graffiti se puede tener una idea.

Podrás encontrar fotos de otros eventos en los que participamos, fiestas aleatorias y hackers reunidos.



-----
Nuestro video de Hackerñol!
==============================
[peertube node](https://video.hispagatos.org/video-channels/c87b5aed-96db-4df5-8643-46507f0f41d4/videos)

<center>
<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://video.hispagatos.org/videos/embed/08b7d267-dfd6-456d-b422-ffc7b7ad9e29" frameborder="0" allowfullscreen></iframe>
</center>

- El black bloc area

![Anarchist Hackers en 35c3](https://fotos.hispagatos.org/_data/i/upload/2019/02/01/20190201023342-698b01d6-me.jpg)


Happy Hacking, Hack the System and keep on dreaming! a(A)a <3
-------------------------------------------------------------

[ReK2](https://keybase.io/rek2)
[VideoShow](https://video.hispagatos.org/videos/local)
[Pictures](https://fotos.hispagatos.org)

---
Traducción por Sigma0100
