---
categories: hispagatos
comments: true
date: 2014-10-21T00:00:00Z
title: our Site is back
url: /2014/10/21/our-site-is-backbundler-exec-rake-t/
---

Yes, *sorry* we were out for 2-3 months, run out of $$$ to renew the domain. But we are back now.
Shortly, we will upload our new projects and other things related to our ideals.  

Don't foget to visit daily news and hot topics at [BinaryFreedom](http://binaryfreedom.info).

**Thanks**
