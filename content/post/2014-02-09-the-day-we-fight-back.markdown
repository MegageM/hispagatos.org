---
categories: Hacktivism
comments: true
date: 2014-02-09T00:00:00Z
title: The Day we Fight Back
url: /2014/02/09/the-day-we-fight-back/
---

<p>
We are proud to be taking part in the Day we Fight Back action across cyberspace.
</p>
<b>DEAR USERS OF THE INTERNET,</b>
<p>
In January 2012 we defeated the SOPA and PIPA censorship legislation with the largest Internet protest in history. Today we face another critical threat, one that again undermines the Internet and the notion that any of us live in a genuinely free society: mass surveillance.
</p>
<p>
In celebration of the win against SOPA and PIPA two years ago, and in memory of one of its leaders, Aaron Swartz, we are planning a day of protest against mass surveillance, to take place this February 11th.
</p>
<p>
Together we will push back against powers that seek to observe, collect, and analyze our every digital action. Together, we will make it clear that such behavior is not compatible with democratic governance. Together, if we persist, we will win this fight.
</p>
