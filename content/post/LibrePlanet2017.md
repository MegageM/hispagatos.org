+++
date = "2017-03-29T11:51:24-07:00"
title = "LibrePlanet2017"
draft = false

+++

<h1> Hispagatos at LibrePlanet 2017 </h1>
<p>Hello, ReK2 [ReK2](https://keybase.io/cfernandez)  [talk](https://libreplanet.org/2017/program/speakers.html#fernandez) at [LibrePlanet 2017 in MIT](https://libreplanet.org/2017/), Boston MA was done
this last Saturday, here below are the slideshows, of course the slides
do not have the whole thing since is only a overview the talk was a lot richer
and with more content but you will get the idea..</p>

[link to video](https://media.libreplanet.org/u/libreplanet/m/pentesting-loves-free-software/)
[link to slides](https://www.slideshare.net/ChrisFernandezCEHv7I/penetrationtestinglovesfreesoftware-libreplaner2017christianfernandezhispagatos)

<iframe src="//www.slideshare.net/slideshow/embed_code/key/1MFwc2ZYy87uw7" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/ChrisFernandezCEHv7I/penetrationtestinglovesfreesoftware-libreplaner2017christianfernandezhispagatos" title="Penetrationtestinglovesfreesoftware libreplaner2017-christianfernandez-hispagatos" target="_blank">Penetrationtestinglovesfreesoftware libreplaner2017-christianfernandez-hispagatos</a> </strong> from <strong><a target="_blank" href="//www.slideshare.net/ChrisFernandezCEHv7I">Chris Fernandez CEHv7, eCPPT, Linux Engineer</a></strong> </div>

