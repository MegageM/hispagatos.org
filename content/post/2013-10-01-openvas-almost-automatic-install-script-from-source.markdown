---
categories: software
comments: true
date: 2013-10-01T00:00:00Z
title: OpenVas - Almost automatic install Script from source
url: /2013/10/01/openvas-almost-automatic-install-script-from-source/
---

<p>Ok well a couple weeks a go I decided to install OpenVas for one of my clients..</p>
<p>seen that their stable releases were not that updated, and they are working already
in a new stable release(in beta as i speak) I decided to make it easy for people
and myself to be able to repeat the procedure.</p>
<p>So I created this script, is not finish/perfect but I will release it anyhow in hopes
more people will jump in and help out, clean and make it more usefull.
As it is is very barebones.. lot of things to improve.</p>
<br>
<p>here is the link:</p>
https://github.com/HispaGatos/openvas_install
<p>feel free to FORK! and help out<p>
<p>happy hacking.</p>
