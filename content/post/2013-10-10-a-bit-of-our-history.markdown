---
categories: info
comments: true
date: 2013-10-10T00:00:00Z
title: A bit of our History
url: /2013/10/10/a-bit-of-our-history/
---

<p>We go all the way back to 2001, when <b>Christian Fernandez</b> and <b>Jeff Grant</b> both very active in Free Software
activism formed the <b>"Boston MetroWest GNU/Linux user group"</b> we had a very good success in organizing
not so serious meetups in bars and restaurants.. some people like <b>Karl hiramoto</b> join us, we tried to be the less formal of the Worcester and Boston LUG's
and we were good at it :) </p>
<p>A couple of years laters our mailing list was flooded by political talk about software freedom, digital rights,
decentralization of the internet and the world arround us.. so at the moment we switched from been a LUG to be
a full force activist group, caled the <b>"Independent Free Software Activists"</b><b>IFSA</b>.</p>
<p>We had actual real social justice activist to join us, so after a couple years we had no doub.. we were 
a strong and committed group, We meet more people like  <b>Angela Pineros</b> <b>Christopher Parker</b> <b>Danny</b> <b>Deborah</b><b>Ringo</b> and many more..
and this is when <b>BinaryFreedom</b> was born!</p>
We had a lot of press, and we started to join with the <a href="http://fsf.org">FSF</a> <b>John Sullivan</b> <b></b>and work closely, while they were
doing for most part at the time the office and tech work, we were in the streets, protesting and giving out CD's and stickers,
<a href="http://www.freesoftwaremagazine.com/articles/interview_christian_fernandez_binary_freedom#!">here a small interview from the time</a>
and an <a href="http://www.freesoftwaremagazine.com/node/2222#!">example of one of our actions </a></p>
<p>After a couple years, the FSF started to do more street actions and we kind of just joined them in full force, we also
started to do social justice activism and anti-war actions but later on,  most of us left
to other places in the planet.. others got married and had kids..like <b>Jeff Grant</b> or moved to San Francisco like <b>Christian Fernandez</b>
That period of activism was great! later on <b>Christian Fernandez</b> started to get deep into hacking and hacktivism, moving what he 
learn from the last period to online activism, and now he is moving around the world, at the moment in Barcelona, yet starting
another groub paralell to <a href="http://binaryfreedom.info">BinaryFreedom</a> to bring together great people and great ideas...
Hispagatos LABS. there is no clear road yet other than getting a bunch of people together, the road they take is still to be seen.<p>

