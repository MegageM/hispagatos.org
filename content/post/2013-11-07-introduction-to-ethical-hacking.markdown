---
categories: workshop
comments: true
date: 2013-11-07T00:00:00Z
title: Introduction to Ethical Hacking
url: /2013/11/07/introduction-to-ethical-hacking/
---

<p>Event:
Iniciación al Ethical Hacking</p>
<p>Start:
06/11/2013 6:00 pm</p>
<p>End:
06/11/2013 7:30 pm</p>
<p>Makers of Barcelona.</p> <p>http://www.mob-barcelona.com/event/iniciacion-al-ethical-hacking/</p>

  <p>Una introducción  al Hacking Ético, el cual consiste en un ataque controlado a los sistemas informáticos y/o de comunicaciones de una empresa u organización empleando los mismos medios que un usuario malicioso. Se llama “Hacking Ético” debido a que las técnicas, pensamientos y metodologías usadas son similares a las empleadas por los hackers, pero el único objetivo es comprobar el estado real y actual de la seguridad. No confundir con auditorias donde se usan herramientas preparadas para “testear” la vulnerabilidad de los servicios del cliente y reportar el error. Como Hacker Ético, lo primero que debes aprender es a pensar como un hacker malicioso, con mucha creatividad y a no dar nada por supuesto. Durante el workshop abordaremos ciertas teorías y herramientas y si es posible, también  vulnerabilidades ya conocidas, para que la gente se inicie.</p>

{% slideshare 28003672%}
